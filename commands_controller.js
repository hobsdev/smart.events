var datastore = require('./datastore'),
    util = require('./util');

module.exports = {
    'login': function (message, socket) {
        console.log('Login received.');
        datastore.setData(message.clientId, socket);
    },
    'listClients': function (message) {
        var sender = datastore.getByKey(message.clientId);
        if (!sender) return;

        // finds all connected clientes
        var currentClients = datastore.all().map(function (i) {
            return {
                'id': i
            }
        });

        // build the response
        response = util.buildServerMessage(
            'get_clients',
            currentClients,
            message.obj.requestId);

        // writes the response to client socket
        sender.write(response);
    },
    'getClient': function (message) {
        sender = datastore.clients.get(message.clientId);
        if (!sender) return;

        // build the response
        response = util.buildServerMessage(
            'get_client',
            util.getClientById(message.obj.clientId),
            message.obj.requestId);

        // writes the response to client socket
        sender.write(response);
    },
    'send': function (message) {
        var sender = datastore.getByKey(message.clientId);
        targetClient = datastore.clients.get(message.obj.clientId);
        obj = {};

        // if the target is connected
        if (targetClient)
        {
            // send an execute command to a target client
            targetMessage = {
                'command': 'execute',
                'clientId': 'server',
                'obj': message.obj.executable
            };
            targetClient.write(JSON.stringify(targetMessage));

            // sets the response
            obj = { 'status': 'sent' };
        }
        else
        {
            // if the target client is not conncted
            obj = {
                'error': {
                    'message': 'Target Client not found.'
                },
                'status': 'not_sent'
            };
        }

        // build the response to the api
        response = util.buildServerMessage('send', obj, message.obj.requestId);

        // writes the response at api socket
        sender.write(JSON.stringify(response));
    },
    'sendAll': function (message) {
        sender = datastore.clients.get(message.clientId);
        response = {
            'command': 'send_all',
            'clientId': 'server',
            'obj': {
                'status': 'sent'
            }
        };

        datastore.all().forEach(function (k) {
            targetClient = datastore.clients.get(k);

            if (targetClient)
            {
                targetMessage = {
                    'command': 'execute',
                    'clientId': 'server',
                    'obj': message.obj.executable
                };
                targetClient.write(JSON.stringify(targetMessage));
            }
        });

        if (message.obj.requestId)
            response.requestId = message.obj.requestId;

        sender.write(JSON.stringify(response));
    }
};
