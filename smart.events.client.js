var net = require('net');
var os = require('os');
var shell = require('shelljs');
var sleep = require('sleep');
var HOST = process.env.SERVER_HOST || '0.0.0.0';
var PORT = process.env.SERVER_PORT || 3000;
var retries = 0;
var maxRetries = 5;

var clientId = generateClientId();
var client = net.Socket();


client.on('connect', function () {
    console.log('Connected with clientId: %s', clientId);
    sendMessage('login', {
        'platform': os.platform()
    });
});

client.on('data', function (data) {
    message = readMessage(data);
    if (message == null)
        return;

    if (message.clientId == clientId)
        console.log('My message. Do nothing.');
    else
    {
        console.log('Received from server.');
        if (message.command == 'execute')
        {
            console.log('Executing command: %s', message.obj)
            var output = shell.exec(message.obj, {silent: true}).stdout;
            if (typeof(output) !== undefined)
                console.log(output);
            console.log('Done.');
        }
    }
});

client.on('close', function (had_error) {
    client.destroy();
    retries++;
    connectClient(retries);
    console.log('Bye. Connection closed.');
});

client.on('error', function (err) {
    quitWithError();
});

connectClient(client, 0);


function quitWithError() {
    console.log('Error: Could not connect to %s:%s', HOST, PORT);
}

function sendMessage(command, obj) {
    message = {
        'command': command,
        'clientId': clientId,
        'obj': obj
    };
    m = JSON.stringify(message);
    client.write(m);
}

function readMessage(message) {
    try {
        return JSON.parse(message);
    } catch (e) {
        return null;
    }
}

function generateClientId() {
    var hostname = process.env.USER;
    return hostname + ':' + randomString(4);
}

function randomString(length) {
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

function connectClient(retries) {
    if (retries >= 0)
    {
        console.log('Retrying to connect %s x in 5 seconds...', retries);
        sleep.sleep(5);
    }

    if (retries > maxRetries)
    {
        quitWithError();
        process.exit();
    }

    client.connect(PORT, HOST);
}
