//
var net = require('net'),
    HashMap = require('hashmap'),
    util = require('./util'),
    cliparser = require('./cliparser'),
    datastore = require('./datastore'),
    commandsController = require('./commands_controller');

var HOST = process.env.SERVER_HOST || '0.0.0.0';
var PORT = process.env.SERVER_PORT || 3000;

// Creates a new hash which will store all online clients
var clients = new HashMap();

// Starts listening the console in order to run commands on-demand
//  via command-line interface
cliparser.init();

// Creates a new server object
var server = net.createServer();

// Event triggered when the server starts listening
server.on('listening', function () {
    console.log('Smart OS Events Server.');
    console.log('Listening on %s:%s...', HOST, PORT);
});

// Event triggered when a client is connected to server
//  All events received by this socket is related to the client
//  whose has just connected
server.on('connection', function (socket) {
    console.log('Client connected!');
    var socketClientId = '';

    // Event triggered when the server receives some data from a
    //  socket
    socket.on('data', function (data) {

        reader = util.readMessage(data, socket);
        socketClientId = reader.socketClientId;

        // defines the api handlers
        reader
            .on('login', commandsController.login)
            .on('send', commandsController.send)
            .on('send_all', commandsController.sendAll)
            .on('get_clients', commandsController.listClients)
            .on('get_client', commandsController.getClient)
            .parse();

        console.log('Message received from client %s', reader.socketClientId);
    });

    // Event triggered when a socket is disconnected
    socket.on('end', function () {
        console.log('Client %s disconnected!', socketClientId);
        // removes it from the clients collection
        datastore.clients.remove(socketClientId);
    });
});

// Event triggered an error occurs with the server's connection
server.on('error', function (err) {
    console.log('Error: Could not bind the address %s:%s', HOST, PORT);
});

// Listens to the source port and host
server.listen(PORT, HOST);
