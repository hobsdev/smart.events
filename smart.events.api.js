var net = require('net');
var express = require('express');
var bodyParser = require('body-parser');
var HashMap = require('hashmap');
var app = express();
var SERVER_HOST = process.env.SERVER_HOST || '0.0.0.0';
var SERVER_PORT = process.env.SERVER_PORT || 3000;
var PORT = process.env.API_PORT || 5000;

var clientId = 'api';
var client = net.Socket();
var requests = new HashMap();

// same on client-core.js
client.on('connect', function () {
    console.log('Connected with clientId: %s', clientId);
    sendMessage('login', {
        'platform': 'api'
    });
});

client.on('data', function (data) {
    message = readMessage(data);
    if (message == null)
        return;

    if (message.clientId == clientId)
        console.log('My message. Do nothing.');
    else
    {
        console.log('Received from server.');
        if (message.requestId)
        {
            requests.set(message.requestId, message);
        }
    }
});

client.on('close', function (had_error) {
    client.destroy();
    console.log('Bye. Connection closed.');
});

client.on('error', function (err) {
    quitWithError();
});

client.connect(SERVER_PORT, SERVER_HOST);




app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', function (req, res) {
    return res.send({
        'environment': 'dev',
        'version': '1.0.0'
    });
});

app.get('/clients', function (req, res) {
    sendMessageToServer('get_clients', {}, function (response) {
        return res.send(response.obj);
    });
});

app.get('/clients/:id', function (req, res) {
    params = {
        'clientId': req.params.id
    };
    sendMessageToServer('get_client', params, function (response) {
        return res.send(response.obj);
    });
});

app.post('/commands', function (req, res) {
    params = req.body;
    var command = params.target == 'client' ? 'send' : 'send_all';

    sendMessageToServer(command, params, function (response) {
        return res.send(response.obj);
    });
});


// move to utils.
function quitWithError() {
    console.log('Error: Could not connect to %s:%s', SERVER_HOST, SERVER_PORT);
}

// move to client-core.js
function sendMessage(command, obj) {
    message = {
        'command': command,
        'clientId': clientId,
        'obj': obj
    };

    m = JSON.stringify(message);
    client.write(m);
}

function sendMessageToServer(command_type, params, cb) {
    var requestId = randomString(10);
    requests.set(requestId, null);

    params.requestId = requestId;

    sendMessage(command_type, params);

    var i = setInterval(function () {
        var response = requests.get(requestId);
        if (response != null)
        {
            clearInterval(i);
            cb(response);
        }
    }, 100);
}

function readMessage(message) {
    try {
        return JSON.parse(message);
    } catch (e) {
        return null;
    }
}

function randomString(length) {
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
    for(var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

var server = app.listen(PORT, function () {
    console.log('Smart OS Events API.');
    console.log("Listening on port %s...", server.address().port);
});

