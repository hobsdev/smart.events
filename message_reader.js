var HashMap = require('hashmap');

function MessageReader(message, socket, e) {
    if (message != null) {
        this.message = message;
        this.events = new HashMap();
        this.socketClientId = message.clientId;
        this.socket = socket;
    }
    else
        this.error = e;
}

MessageReader.prototype.on = function on(command, cb) {
    this.events.set(command, cb);
    return this;
};

MessageReader.prototype.parse = function parse() {
    var cb = this.events.get(this.message.command);
    if (cb)
        cb(this.message, this.socket);
};

MessageReader.prototype.setSocket = function setSocket(socket) {
    this.socket = socket;
};

module.exports = MessageReader;
