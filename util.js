var MessageReader = require('./message_reader'),
    datastore = require('./datastore'),
    os = require('os');

module.exports = {
    'readMessage': function (message, socket) {
        try {
            return new MessageReader(JSON.parse(message.toString()), socket);
        } catch (e) {
            return new MessageReader(null, null, e);
        }
    },
    'buildServerMessage': function (command, executable, requestId) {
        message = {
            'command': command,
            'clientId': 'server',
            'obj': executable
        };

        if (requestId)
            message.requestId = requestId;

        return JSON.stringify(message);
    },
    'getClientById': function (clientId) {
        selectedClient = datastore.clients.get(clientId);
        if (selectedClient)
        {
            return {
                'id': clientId,
                'memory': {
                    'total': os.totalmem(),
                    'free': os.freemem()
                },
                'uptime': os.uptime()
            };
        }
        else
        {
            return {
                'error': {
                    'message': 'Client not found.'
                }
            };
        }
    }
};
