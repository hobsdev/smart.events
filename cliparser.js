var readline = require('readline'),
    commandParser = require('./command_parser'),
    datastore = require('./datastore'),
    util = require('./util');

module.exports = {
    'init': function() {
        this.initCommandParser();
        this.initReadline();
    },
    'initReadline': function () {
        var rl = readline.createInterface({
          input: process.stdin,
          output: process.stdout
        });

        rl.on('line', function (input) {
            commandParser.execute(input);
        });
    },
    'initCommandParser': function () {
        commandParser
            // when a list command is intercepted by CLI
            .on('list', function () {
                console.log('Listing all clients...');
                datastore.all().forEach(function (k) {
                    console.log('-> ' + k);
                });
            })
            // when a send command is intercepted by CLI
            .on('send', function (command) {
                // builds a server message to client
                m = util.buildServerMessage('execute', command.executable);

                // tries to find the client
                client = datastore.clients.get(command.clientReceiver)

                // if the client is connected
                if (client != null)
                {
                    // writes the message at clients' socket
                    client.write(m);
                    console.log('Command sent.');
                }
                else
                    console.log('Client %s not found', command.clientReceiver);
            });
    }
};
