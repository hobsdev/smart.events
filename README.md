smart.events
==============

Setup
-----------

*Server*

1. Setup the environment variables SERVER_HOST and SERVER_PORT. Usually the SERVER_HOST will be set as 0.0.0.0 for binding all possible addresses. The SERVER_PORT should be exposed and accessible via TCP protocol. If no port is specified, the default port is 3000.

2. In order to start the server, run:
```
node smart.events.server.js
```

3. Server Commands

With the server running, you are able for using the two commands below:

'l' command for listing the users connected:
```
l
Listing all clients...
-> miani:bxr0
-> miani:5x31
```

'send' command for executing a shell at the target client.
```
send miani:bxr0 touch a.txt
Command sent.
```

'exit' and 'clear' are also recognized.



*API*

1. Setup the env vars

Endpoints
----

1. List all connected clients
GET http://localhost:5000/clients
Response:
[
  {
    "id": "api"
  },
  {
    "id": "miani:bblp"
  },
  {
    "id": "miani:ni5w"
  }
]

2. Get information about a client
GET http://localhost:5000/clients/:clientId

3. Send a command to a particular client

Request:
POST http://localhost:5000/commands
{
    "target": "client",
    "clientId": "miani:f9wz",
    "executable": "touch api.txt"
}

No success
{
  "error": {
    "message": "Target Client not found."
  },
  "status": "not_sent"
}

With success:
{
  "status": "sent"
}


4. Send a command to all connected clients

POST http://localhost:5000/commands
{
    "target": "all",
    "executable": "touch all.txt"
}
