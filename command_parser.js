module.exports = {
    'parse': function (message) {
        message = message.split(' ');
        command_type = message[0];
        result = {
            'command_type': command_type,
            'success': true
        };

        if (command_type == 'send')
        {
            if (message.length >= 3)
            {
                result.clientReceiver = message[1];
                result.executable = message.slice(2).join(' ');
            }
            else
            {
                return {
                    success: false,
                    error: 'Invalid command. Usage: send [client_id] [command]'
                }
            }
        }

        return result;
    },
    'execute': function (message) {
        command = this.parse(message);
        if (command.success) {
            command_type = command.command_type;

            if (command_type == 'l')
                this.list();
            else if (command_type == 'send')
                this.send(command);
            else if (command_type == 'clear')
                this.clear();
            else if (command_type == 'exit')
                this.exit();
            else
                this.unknownCommand(command_type);
        }
        else
            this.unknownCommand(command.error);
    },
    'clear': function () {
        console.log('\033[2J');
    },
    'exit': function () {
        console.log('Exiting...Bye.');
        process.exit();
    },
    'unknownCommand': function (command) {
        console.log('Unknown command %s', command);
    },
    'on': function (event, callback) {
        if (event == 'list')
            this.list = callback;
        else if (event == 'send')
            this.send = callback;
        return this;
    }
};
