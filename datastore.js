var HashMap = require('hashmap');

module.exports = {
    'clients': new HashMap(),
    'all': function () {
        return this.clients.keys();
    },
    'getByKey': function (key) {
        return this.clients.get(key);
    },
    'setData': function (key, value) {
        this.clients.set(key, value);
    }
};
